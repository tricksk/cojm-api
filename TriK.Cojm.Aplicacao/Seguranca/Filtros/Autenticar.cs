﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using TriK.Cojm.Aplicacao.Seguranca.Enumeradores;

namespace TriK.Cojm.Aplicacao.Seguranca.Filtros
{
    public class Autenticar : Attribute, IAuthenticationFilter
    {
        private readonly TipoSessaoEnum tipoSessaoEnum;

        public Autenticar(TipoSessaoEnum tipoSessaoEnum)
        {
            this.tipoSessaoEnum = tipoSessaoEnum;
        }


        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            string token = context.Request.Headers.GetValues("Token").FirstOrDefault();

            switch(tipoSessaoEnum)
            {
                case TipoSessaoEnum.Adm:
                    break;
                case TipoSessaoEnum.Site:
                    break;
            }
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
        }

        public bool AllowMultiple { get; }
    }
}
